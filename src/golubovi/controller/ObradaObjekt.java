/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.controller;

import golubovi.model.Objekt;
import golubovi.utility.SqlBaza;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Izidora i Mario
 */
public class ObradaObjekt extends Obrada<Objekt>{
   

    @Override
    public Objekt dodajNovi(Objekt entitet) {
         
    try{
             Connection veza = SqlBaza.getConnection();
        PreparedStatement izraz = veza.prepareStatement("insert into objekt (naziv, lokacija, vlasnik, velicina, broj_prostorija, broj_etaza) values (?,?,1,?,?,?)",Statement.RETURN_GENERATED_KEYS);
        
        veza.setAutoCommit(false);
        izraz.setString(1, entitet.getNaziv());
         izraz.setString(2, entitet.getLokacija());
          izraz.setInt(3, entitet.getVelicina());
          izraz.setInt(4, entitet.getBroj_prostorija());
          izraz.setInt(5, entitet.getBroj_etaza());
       
          izraz.executeUpdate();
        ResultSet rs = izraz.getGeneratedKeys();
        rs.next();
        entitet.setSifra(rs.getInt(1));
        rs.close();
        izraz.close();
        veza.commit();
        veza.close();
        }catch(Exception e){
          
            e.printStackTrace();
            return null;
        }
       return entitet;
    }

    @Override
    public void promjeniPostojeci(Objekt entitet) {
         try{
             try (Connection veza = SqlBaza.getConnection()) {
                 veza.setAutoCommit(false);
                 try (PreparedStatement izraz = veza.prepareStatement("update objekt set naziv=?, lokacija=?, velicina=?, broj_prostorija=?, broj_etaza=? where sifra =?")) {
                     izraz.setString(1, entitet.getNaziv());
                     izraz.setString(2, entitet.getLokacija());
                     izraz.setInt(3, entitet.getVelicina());
                     izraz.setInt(4, entitet.getBroj_prostorija());
                     izraz.setInt(5, entitet.getBroj_etaza());
                     izraz.setInt(6, entitet.getSifra());
                     
                     izraz.executeUpdate();
                     izraz.close();
                 }
                 veza.commit();
                 veza.close();
             } catch (ClassNotFoundException | IOException ex) {
                 
                 
                 Logger.getLogger(ObradaObjekt.class.getName()).log(Level.SEVERE, null, ex);
             }
        }catch(SQLException e){
            
             
        }
    }

    /**
     *
     * @param entitet
     * @throws SQLException
     */
    @Override
    public void obrisiPostojeci(Objekt entitet) throws SQLException{
         try{
             Connection veza = SqlBaza.getConnection();
        PreparedStatement izraz = veza.prepareStatement("delete from objekt where sifra=?");
          izraz.setInt(1, entitet.getSifra());
          izraz.executeUpdate();
        izraz.close();
        veza.close();
        }catch( ClassNotFoundException | IOException e){
           //  System.out.println(e.getMessage());
            e.printStackTrace();
            return ;
        } 
    }

    @Override
    public List<Objekt> dohvatiIzBaze(String uvjet) {
        
        List<Objekt> lista = new ArrayList<>();
        try{
             Connection veza = SqlBaza.getConnection();
        Statement izraz = veza.createStatement();
        
        ResultSet rs = izraz.executeQuery("select * from objekt");
        Objekt objekt = null;
        
        while (rs.next()) {
           
            objekt = new Objekt();
            objekt.setSifra(rs.getInt("sifra"));
            objekt.setNaziv(rs.getString("naziv"));
            objekt.setBroj_etaza(rs.getInt("broj_etaza"));
            objekt.setBroj_prostorija(rs.getInt("broj_prostorija"));
            objekt.setLokacija(rs.getString("lokacija"));
            objekt.setVelicina(rs.getInt("velicina"));
            objekt.setVlasnik(rs.getInt("vlasnik"));
            
            lista.add(objekt);
           
        }
        rs.close();
        izraz.close();
        veza.close();
        }catch(IOException | ClassNotFoundException | SQLException e){
           e.printStackTrace();
            return null;
        }
       
        
        return lista;
        
    }

    public List<Objekt> dohvatiSifruIzBaze() {
        
        List<Objekt> lista = new ArrayList<>();
        try{
             Connection veza = SqlBaza.getConnection();
        Statement izraz = veza.createStatement();
        
        ResultSet rs = izraz.executeQuery("select sifra from objekt");
        Objekt objekt = null;
        
        while (rs.next()) {
           
            objekt = new Objekt();
            objekt.setSifra(rs.getInt("sifra"));
           
            
            lista.add(objekt);
           
        }
        rs.close();
        izraz.close();
        veza.close();
        }catch(IOException | ClassNotFoundException | SQLException e){
           e.printStackTrace();
            return null;
        }
       
        
        return lista;
        
    }
    
}

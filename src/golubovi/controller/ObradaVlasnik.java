/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.controller;

import golubovi.model.Vlasnik;
import golubovi.utility.SqlBaza;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Izidora i Mario
 */
public class ObradaVlasnik extends Obrada<Vlasnik>{

   
     public Vlasnik autoriziraj(String korisnickoime, String lozinka) throws FileNotFoundException, ClassNotFoundException, IOException, SQLException {

        Connection veza = SqlBaza.getConnection();
        PreparedStatement izraz = veza.prepareStatement("select * from vlasnik where korisnicko_ime=? and lozinka=?");
        izraz.setString(1, korisnickoime);
        izraz.setString(2, DigestUtils.md5Hex(lozinka));
        ResultSet rs = izraz.executeQuery();
        Vlasnik vlasnik = null;
        while (rs.next()) {
            vlasnik = new Vlasnik();
            vlasnik.setSifra(rs.getInt("sifra"));
            vlasnik.setIme(rs.getString("ime"));
            vlasnik.setPrezime(rs.getString("prezime"));
            vlasnik.setKorisnickoIme(korisnickoime);
            vlasnik.setLozinka(lozinka);
           vlasnik.setAdresa("adresa");
           vlasnik.setEmail("email");
        }
        rs.close();
        izraz.close();
        veza.close();
        
        return vlasnik;
         

    }
    
    

    @Override
    public Vlasnik dodajNovi(Vlasnik entitet) throws FileNotFoundException, ClassNotFoundException, IOException, SQLException {
        
    
             Connection veza = SqlBaza.getConnection();
        PreparedStatement izraz = veza.prepareStatement("insert into vlasnik (korisnicko_ime,lozinka,ime,prezime,adresa,email) values (?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
        
        veza.setAutoCommit(false);
        izraz.setString(1, entitet.getKorisnickoime());
         izraz.setString(2, DigestUtils.md5Hex(entitet.getLozinka()));
          izraz.setString(3, entitet.getIme());
          izraz.setString(4, entitet.getPrezime());
          izraz.setString(5, entitet.getAdresa());
          izraz.setString(6, entitet.getEmail());
          izraz.executeUpdate();
        ResultSet rs = izraz.getGeneratedKeys();
        rs.next();
        entitet.setSifra(rs.getInt(1));
        rs.close();
        izraz.close();
         veza.commit();
        veza.close();
        
       return entitet;
       
    }

    @Override
    public void promjeniPostojeci(Vlasnik entitet) {
         try{
             Connection veza = SqlBaza.getConnection();
             PreparedStatement izraz=null;
             
             veza.setAutoCommit(false);
         izraz = veza.prepareStatement("update vlasnik set ime=?,prezime=?,adresa=?, email=?");
           
            izraz.setString(1, entitet.getIme());
          izraz.setString(2, entitet.getPrezime());
          izraz.setString(3, entitet.getAdresa());
          izraz.setString(4, entitet.getEmail());
         
          izraz.executeUpdate();
          
        izraz.close();
        veza.commit();
        veza.close();
        }catch(Exception e){
           
            e.printStackTrace();
        }
    }
    
    public void promjeniPostojecuLozinku(Vlasnik entitet) {
         try{
             Connection veza = SqlBaza.getConnection();
             PreparedStatement izraz=null;
             
             veza.setAutoCommit(false);
         izraz = veza.prepareStatement("update vlasnik set lozinka=?");
           izraz.setString(1, DigestUtils.md5Hex(entitet.getLozinka()));
         
          izraz.executeUpdate();
          
        izraz.close();
        veza.commit();
        veza.close();
        }catch(Exception e){
           
            e.printStackTrace();
            return ;
        }
    }

    

   // @Override
    public List<Vlasnik> dohvatiIzBaze(String string) throws FileNotFoundException, ClassNotFoundException, IOException, SQLException {
        
         List<Vlasnik> lista = new ArrayList<>();
       
             Connection veza = SqlBaza.getConnection();
        Statement izraz = veza.createStatement();
        
        ResultSet rs = izraz.executeQuery("select * from vlasnik");
        Vlasnik vlasnik = null;
        
        while (rs.next()) {
           
            vlasnik = new Vlasnik();
          vlasnik.setSifra(rs.getInt("sifra"));
            vlasnik.setIme(rs.getString("ime"));
            vlasnik.setPrezime(rs.getString("prezime"));
            vlasnik.setAdresa(rs.getString("adresa"));
            vlasnik.setKorisnickoIme(rs.getString("korisnicko_ime"));
            vlasnik.setEmail(rs.getString("email"));
            vlasnik.setLozinka(rs.getString("lozinka"));
            
            
            lista.add(vlasnik);
           
        }
        rs.close();
        izraz.close();
        veza.close();
       
       
        
        return lista;
        
    }
    

    @Override
    public void obrisiPostojeci(Vlasnik entitet) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    }
    
    
    

   
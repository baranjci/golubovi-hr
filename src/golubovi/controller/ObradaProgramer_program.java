/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.controller;


import golubovi.utility.SqlBaza;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import golubovi.model.Programer_program;
import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Izidora i Mario
 */
public class ObradaProgramer_program {
    
    public Programer_program autoriziraj(String korisnicko_ime_admin, String lozinka_admin) throws FileNotFoundException, ClassNotFoundException, IOException, SQLException {

        Connection veza = SqlBaza.getConnection();
        PreparedStatement izraz = veza.prepareStatement("select * from programer_program where korisnicko_ime_admin=? and lozinka_admin=?");
        izraz.setString(1, korisnicko_ime_admin);
        izraz.setString(2, DigestUtils.md5Hex(lozinka_admin));
        ResultSet rs = izraz.executeQuery();
        Programer_program pp = null;
        while (rs.next()) {
            pp = new Programer_program();
             pp.setKorisnicko_ime_admin(korisnicko_ime_admin);
            pp.setLozinka_admin(lozinka_admin);
            pp.setIme_programer("ime_programera");
           pp.setPrezime_programer("prezime_programera");
        }
        rs.close();
        izraz.close();
        veza.close();
        
        return pp;
         

    }
    
    
     public List<Programer_program> dohvatiIzBaze(String string) {
        
         List<Programer_program> lista = new ArrayList<>();
        try{
             Connection veza = SqlBaza.getConnection();
        Statement izraz = veza.createStatement();
        
        ResultSet rs = izraz.executeQuery("select * from programer_program");
        Programer_program pp = null;
        
        while (rs.next()) {
           
            pp = new Programer_program();
          
            pp.setIme_programer(rs.getString("ime_programer"));
            pp.setPrezime_programer(rs.getString("prezime_programer"));
            pp.setEmail(rs.getString("email"));
            pp.setMobitel(rs.getString("mobitel"));
            pp.setProizvodjac(rs.getString("proizvodjac"));
            pp.setProjekt(rs.getString("projekt"));
            pp.setVerzija_programa(rs.getString("verzija_programa"));
            
            lista.add(pp);
            
        }
        rs.close();
        izraz.close();
        veza.close();
        }catch(IOException | ClassNotFoundException | SQLException e){
            e.printStackTrace();
            return null;
        }
       
        
        return lista;
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package golubovi.controller;

import golubovi.model.Bolesti;
import golubovi.utility.SqlBaza;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Izidora i Mario
 */
public class ObradaBolesti extends Obrada<Bolesti> {

    @Override
    public Bolesti dodajNovi(Bolesti entitet) {
        try {
            Connection veza = SqlBaza.getConnection();
            PreparedStatement izraz = veza.prepareStatement("insert into bolesti (naziv, trajanje_bolesti_od, trajanje_bolesti_do, broj_zarazenih, lijek) values (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

            veza.setAutoCommit(false);
            izraz.setString(1, entitet.getNaziv());
            izraz.setDate(2, entitet.getTrajanje_bolesti_od());
            izraz.setDate(3, entitet.getTrajanje_bolesti_do());
            izraz.setInt(4, entitet.getBroj_zarazenih());
            izraz.setString(5, entitet.getLijek());

            izraz.executeUpdate();
            ResultSet rs = izraz.getGeneratedKeys();
            rs.next();
            entitet.setSifra(rs.getInt(1));
            rs.close();
            izraz.close();
            veza.commit();
            veza.close();
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }
        return entitet;
    }

    @Override
    public void promjeniPostojeci(Bolesti entitet) {
        try {
            try (Connection veza = SqlBaza.getConnection()) {
                veza.setAutoCommit(false);
                try (PreparedStatement izraz = veza.prepareStatement("update bolesti set naziv=?, trajanje_bolesti_od=?, trajanje_bolesti_do=?, broj_zarazenih=?, lijek=? where sifra=?")) {
                    izraz.setString(1, entitet.getNaziv());
                    izraz.setDate(2, entitet.getTrajanje_bolesti_od());
                    izraz.setDate(3, entitet.getTrajanje_bolesti_do());
                    izraz.setInt(4, entitet.getBroj_zarazenih());
                    izraz.setString(5, entitet.getLijek());
                    izraz.setInt(6, entitet.getSifra());
                    izraz.executeUpdate();
                    izraz.close();
                }
                veza.commit();
                veza.close();
            } catch (ClassNotFoundException | IOException ex) {

               
            }
        } catch (SQLException e) {

           
        }
    }

    @Override
    public void obrisiPostojeci(Bolesti entitet) {
        try {
            Connection veza = SqlBaza.getConnection();
            PreparedStatement izraz = veza.prepareStatement("delete from bolesti where sifra=?");
            izraz.setInt(1, entitet.getSifra());
            izraz.executeUpdate();
            izraz.close();
            veza.close();
        } catch (ClassNotFoundException | IOException e) {
            //  System.out.println(e.getMessage());
            e.printStackTrace();
            return;
        } catch (SQLException ex) {

        }
    }

    @Override
    public List<Bolesti> dohvatiIzBaze(String uvjet) {

        List<Bolesti> lista = new ArrayList<>();
        try {
            Connection veza = SqlBaza.getConnection();

            PreparedStatement izraz = veza.prepareStatement("select * from bolesti where naziv like ?");

            izraz.setString(1, "%" + uvjet + "%");

            ResultSet rs = izraz.executeQuery();
            Bolesti bolesti;

            while (rs.next()) {

                bolesti = new Bolesti();
                bolesti.setSifra(rs.getInt("sifra"));
                bolesti.setNaziv(rs.getString("naziv"));
                bolesti.setTrajanje_bolesti_od(rs.getDate("trajanje_bolesti_od"));
                bolesti.setTrajanje_bolesti_do(rs.getDate("trajanje_bolesti_do"));
                bolesti.setBroj_zarazenih(rs.getInt("broj_zarazenih"));
                bolesti.setLijek(rs.getString("lijek"));

                lista.add(bolesti);

            }
            rs.close();
            izraz.close();
            veza.close();
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }

        return lista;
    }
}

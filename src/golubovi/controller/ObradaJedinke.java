/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.controller;

import golubovi.model.Jedinke;
import golubovi.model.Objekt;
import golubovi.utility.SqlBaza;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Izidora i Mario
 */
public class ObradaJedinke extends Obrada<Jedinke>{

    @Override
    public Jedinke dodajNovi(Jedinke entitet) throws NullPointerException{
        try{
             Connection veza = SqlBaza.getConnection();
        PreparedStatement izraz = veza.prepareStatement("insert into jedinke (broj_prstena, boja, spol, slika, objekt, jato, datum_rodjenja) values (?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
        
        veza.setAutoCommit(false);
        izraz.setString(1, entitet.getBroj_prstena());
         izraz.setString(2, entitet.getBoja());
          izraz.setString(3, entitet.getSpol());
          izraz.setString(4, entitet.getSlika());
           izraz.setInt(5, entitet.getObjekt().getSifra());
           izraz.setString(6, entitet.getJato());
           izraz.setDate(7, entitet.getRodjenje());
          
       
          izraz.executeUpdate();
        ResultSet rs = izraz.getGeneratedKeys();
        rs.next();
        entitet.setSifra(rs.getInt(1));
        rs.close();
        izraz.close();
       veza.commit();
        veza.close();
        }catch(Exception e){
          
            e.printStackTrace();
            return null;
        }
       return entitet;
    }

    @Override
    public List<Jedinke> dohvatiIzBaze(String uvjet) {
        
         List<Jedinke> lista = new ArrayList<>();
        try{
             Connection veza = SqlBaza.getConnection();
       PreparedStatement izraz = veza.prepareStatement("select a.*, b.sifra as sifraobjekta, b.naziv from jedinke a inner join objekt b on a.objekt = b.sifra where broj_prstena like ? or naziv like ?");
         izraz.setString(1, "%" + uvjet + "%");
         izraz.setString(2, "%" + uvjet + "%");
       
       ResultSet rs = izraz.executeQuery();
        
        Jedinke entitet;
       Objekt objekt; 
        while (rs.next()) {
           
            entitet = new Jedinke();
            entitet.setSifra(rs.getInt("sifra"));
            entitet.setBroj_prstena(rs.getString("broj_prstena"));
            entitet.setBoja(rs.getString("boja"));
            entitet.setRodjenje(rs.getDate("datum_rodjenja"));
            entitet.setSpol(rs.getString("spol"));
            entitet.setSlika(rs.getString("slika"));
            entitet.setJato(rs.getString("jato"));
            
          objekt = new Objekt();
          objekt.setSifra(rs.getInt("sifraobjekta"));
          objekt.setNaziv(rs.getString("naziv"));
            
            
           entitet.setObjekt(objekt);
           
            lista.add(entitet);
            
           
        }
        rs.close();
        izraz.close();
        veza.close();
        }catch(IOException | ClassNotFoundException | SQLException e){
           e.printStackTrace();
            return null;
        }
       
        
        return lista;
        
    }
    
    public List<Jedinke> dohvatiSifruIzBaze() {
        
         List<Jedinke> lista = new ArrayList<>();
        try{
             Connection veza = SqlBaza.getConnection();
       PreparedStatement izraz = veza.prepareStatement("select sifra from jedinke");
        
       
       ResultSet rs = izraz.executeQuery();
        
        Jedinke entitet;
  
        while (rs.next()) {
           
            entitet = new Jedinke();
            entitet.setSifra(rs.getInt("sifra"));
           
           
            lista.add(entitet);
            
           
        }
        rs.close();
        izraz.close();
        veza.close();
        }catch(IOException | ClassNotFoundException | SQLException e){
           e.printStackTrace();
            return null;
        }
       
        
        return lista;
        
    }
    
     public List<Jedinke> dohvatiBrojPrstenaIzBaze() {
        
         List<Jedinke> lista = new ArrayList<>();
        try{
             Connection veza = SqlBaza.getConnection();
       PreparedStatement izraz = veza.prepareStatement("select broj_prstena from jedinke");
        
       
       ResultSet rs = izraz.executeQuery();
        
        Jedinke entitet;
  
        while (rs.next()) {
           
            entitet = new Jedinke();
            entitet.setBroj_prstena(rs.getString("broj_prstena"));
           
           
            lista.add(entitet);
            
           
        }
        rs.close();
        izraz.close();
        veza.close();
        }catch(IOException | ClassNotFoundException | SQLException e){
           e.printStackTrace();
            return null;
        }
       
        
        return lista;
        
    }
    
        public List<Jedinke> dohvatiSlikuIzBaze() {
        
         List<Jedinke> lista = new ArrayList<>();
        try{
             Connection veza = SqlBaza.getConnection();
       PreparedStatement izraz = veza.prepareStatement("select slika from jedinke");
        
       
       ResultSet rs = izraz.executeQuery();
        
        Jedinke entitet;
  
        while (rs.next()) {
           
            entitet = new Jedinke();
            entitet.setSlika(rs.getString("slika"));
           
           
            lista.add(entitet);
            
           
        }
        rs.close();
        izraz.close();
        veza.close();
        }catch(IOException | ClassNotFoundException | SQLException e){
           e.printStackTrace();
            return null;
        }
       
        
        return lista;
        
    }
     
    @Override
    public void promjeniPostojeci(Jedinke entitet) {
       try{
             try (Connection veza = SqlBaza.getConnection()) {
                 veza.setAutoCommit(false);
                 try (PreparedStatement izraz = veza.prepareStatement("update jedinke set broj_prstena=?, boja=?, spol=?, datum_rodjenja=?, slika=?, objekt=?, jato=? where sifra=?")) {
                     izraz.setString(1, entitet.getBroj_prstena());
                      izraz.setString(2, entitet.getBoja());
                      izraz.setString(3, entitet.getSpol());
                      izraz.setDate(4, entitet.getRodjenje());
                    izraz.setString(5, entitet.getSlika());
                    izraz.setInt(6, entitet.getObjekt().getSifra());
                    izraz.setString(7, entitet.getJato());
                     izraz.setInt(8, entitet.getSifra());
                     izraz.executeUpdate();
                     izraz.close();
                 }
                 veza.commit();
                 veza.close();
             } catch (ClassNotFoundException | IOException ex) {
                 
                 
                 Logger.getLogger(ObradaObjekt.class.getName()).log(Level.SEVERE, null, ex);
             }
        }catch(SQLException e){
            
            
        }
    }

    @Override
    public void obrisiPostojeci(Jedinke entitet) {
        try{
             Connection veza = SqlBaza.getConnection();
        PreparedStatement izraz = veza.prepareStatement("delete from jedinke where sifra=?");
          izraz.setInt(1, entitet.getSifra());
          izraz.executeUpdate();
        izraz.close();
        veza.close();
        }catch( ClassNotFoundException | IOException e){
           //  System.out.println(e.getMessage());
            e.printStackTrace();
            return ;
        } catch (SQLException ex) {
            Logger.getLogger(ObradaObjekt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void obrisiSlikuIzBaze(Jedinke entitet) {
        try{
             Connection veza = SqlBaza.getConnection();
        PreparedStatement izraz = veza.prepareStatement("update jedinke set slika = null where sifra = ?");
          izraz.setInt(1, entitet.getSifra());
          izraz.executeUpdate();
        izraz.close();
        veza.close();
        }catch( ClassNotFoundException | IOException e){
           //  System.out.println(e.getMessage());
            e.printStackTrace();
            return ;
        } catch (SQLException ex) {
            Logger.getLogger(ObradaObjekt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

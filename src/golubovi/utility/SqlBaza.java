/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.utility;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Izidora i Mario
 */
public class SqlBaza {
    
    public static Connection getConnection() throws FileNotFoundException, ClassNotFoundException, IOException, SQLException {
        Properties properties = Singleton.getProperties();
        Class.forName("com.mysql.jdbc.Driver");
        // Setup the connection with the DB
        return DriverManager
                .getConnection("jdbc:mysql://" + properties.getProperty("server", "localhost") + "/"
                + properties.getProperty("imebaze", "golubovi") + "?useUnicode=true&characterEncoding=UTF-8&"
                + "user=" + properties.getProperty("korisnik", "root"));
        
        
        
        //+ "&password=" + properties.getProperty("lozinka", "root"));
    
}
}
/*
 * To change t
 @Override
 public int getLength() {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void addDocumentListener(DocumentListener listener) {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void removeDocumentListener(DocumentListener listener) {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void addUndoableEditListener(UndoableEditListener listener) {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void removeUndoableEditListener(UndoableEditListener listener) {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public Object getProperty(Object key) {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void putProperty(Object key, Object value) {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void remove(int offs, int len) throws BadLocationException {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public String getText(int offset, int length) throws BadLocationException {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void getText(int offset, int length, Segment txt) throws BadLocationException {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public Position getStartPosition() {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public Position getEndPosition() {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public Position createPosition(int offs) throws BadLocationException {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public Element[] getRootElements() {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public Element getDefaultRootElement() {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public void render(Runnable r) {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }
 his license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package golubovi.utility;

import golubovi.controller.ObradaProgramer_program;
import golubovi.controller.ObradaVlasnik;
import golubovi.model.Programer_program;
import golubovi.model.Vlasnik;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Izidora i Mario
 */
public class SlanjeEmailPoruka extends javax.swing.JFrame {

    private ObradaProgramer_program opp;
    private Programer_program pp;
    private Vlasnik vlasnik;
    private ObradaVlasnik obradaVlasnik;
    private JFrame obavijest;

    /**
     * Creates new form SlanjeEmailPoruka
     */
    public SlanjeEmailPoruka() {
        initComponents();

        opp = new ObradaProgramer_program();
        pp = new Programer_program();
        vlasnik = new Vlasnik();
        obradaVlasnik = new ObradaVlasnik();
        obavijest = new JFrame();
        txtPoruka.requestFocus();

        emailPosiljatelja();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblEmailVlasnika = new javax.swing.JLabel();
        lblEmailPrimatelja = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnPosalji = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtPoruka = new javax.swing.JTextArea();
        lblVlasnik = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Poruka programeru");
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblEmailVlasnika.setText("E-mail vlasnika: ");

        lblEmailPrimatelja.setText("E-mail primatelja: ");

        jLabel2.setText("Poruka");

        btnPosalji.setText("Pošalji e-mail");
        btnPosalji.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPosaljiMouseClicked(evt);
            }
        });
        btnPosalji.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPosaljiKeyPressed(evt);
            }
        });

        txtPoruka.setColumns(20);
        txtPoruka.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txtPoruka.setRows(5);
        jScrollPane1.setViewportView(txtPoruka);

        lblVlasnik.setText("Vlasnik aplikacije: ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                    .addComponent(btnPosalji, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblEmailVlasnika)
                            .addComponent(lblEmailPrimatelja)
                            .addComponent(jLabel2)
                            .addComponent(lblVlasnik))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(lblVlasnik)
                .addGap(18, 18, 18)
                .addComponent(lblEmailVlasnika)
                .addGap(18, 18, 18)
                .addComponent(lblEmailPrimatelja)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPosalji, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPosaljiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPosaljiKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saljiEmail();
            txtPoruka.setText("");
        }
    }//GEN-LAST:event_btnPosaljiKeyPressed

    private void btnPosaljiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPosaljiMouseClicked
        if (txtPoruka.getText().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Niste upisali poruku! \nMolim upišite poruku prije slanja", "Slanje e-maila", JOptionPane.ERROR_MESSAGE);
            txtPoruka.requestFocus();
            txtPoruka.setBackground(Color.red);

            return;

        } else {
            txtPoruka.setBackground(Color.WHITE);

            if (JOptionPane.showConfirmDialog(rootPane, "Potvrdite slanje e-mail poruke", "Potvrda slanja",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                saljiEmail();
                txtPoruka.setText("");
            }

        }
    }//GEN-LAST:event_btnPosaljiMouseClicked

    /**
     * @param args the command line arguments
     */
    private void emailPosiljatelja() {

        try {
            
           vlasnik = obradaVlasnik.dohvatiIzBaze(null).get(0);
        lblVlasnik.setText("Vlasnik aplikacije: " + vlasnik.getIme() + " " + vlasnik.getPrezime());
        lblEmailVlasnika.setText("E-mail vlasnika: " + vlasnik.getEmail());
        pp = opp.dohvatiIzBaze(null).get(0);
        lblEmailPrimatelja.setText("E-mail primatelja: " + pp.getEmail());
            
        } catch (ClassNotFoundException | IOException | SQLException e) {
        }
        

    }

    private void saljiEmail() {

        try {
            Properties props = new Properties();

            props.put("mail.transport.protocol", "smtps");
            props.put("mail.smtps.host", "smtp.gmail.com");
            props.put("mail.smtps.auth", "true");
            // props.put("mail.smtps.quitwait", "false");

            Session mailSession = Session.getDefaultInstance(props);
            mailSession.setDebug(true);
            javax.mail.Transport transport = mailSession.getTransport();

            MimeMessage message = new MimeMessage(mailSession);
            // message subject
            message.setSubject("Vlasnik aplikacije: " + vlasnik.getIme() + " " + vlasnik.getPrezime() + " " + "(" + lblEmailVlasnika.getText() + ")");
            // message body
            message.setContent(txtPoruka.getText(),
                    "text/html");

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(pp.getEmail()));

            transport.connect("smtp.gmail.com", 465, pp.getEmail(), "homePrograming");

            transport.sendMessage(message,
                    message.getRecipients(Message.RecipientType.TO));
            transport.close();

            setVisible(false);
            JOptionPane.showMessageDialog(rootPane, "Poruka je uspješno poslana!", "Slanje e-maila", JOptionPane.INFORMATION_MESSAGE);

            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPosalji;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblEmailPrimatelja;
    private javax.swing.JLabel lblEmailVlasnika;
    private javax.swing.JLabel lblVlasnik;
    private javax.swing.JTextArea txtPoruka;
    // End of variables declaration//GEN-END:variables

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package golubovi.utility;

/**
 *
 * @author Izidora i Mario
 */
public class Kontrola {

    /**
     * kontrola unosa osobnih podataka
     */
    public static final String KORISNICKO_IME_PATTERN = "^[a-zA-Z 0-9]{1,8}$";
    public static final String IME_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ]{2,50}$";
    public static final String PREZIME_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ]{2,50}$";
    public static final String ADRESA_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ 0-9 ,]{2,50}$";
    public static final String LOZINKA_PATTERN = "^[a-zA-Z 0-9]{1,8}$";
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+\n" + "(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$";

    /**
     * kontrola unosa objekta (golubinjaka)
     */
    public static final String LOKACIJA_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ 0-9 ,]{2,50}$";
    public static final String NAZIV_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ 0-9]{2,50}$";
    public static final String VELICINA_PATTERN = "^[0-9]{1,11}$";
    public static final String BROJ_PROSTORIJA_PATTERN = "^[0-9]{1,11}$";
    public static final String BROJ_ETAZA_PATTERN = "^[0-9]{1,11}$";

    /**
     * kontrola unosa jedinki (golubova)
     */
    public static final String BROJ_PRSTENA_PATTERN = "^[a-zA-Z 0-9]{2,250}$";
    public static final String BOJA_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ]{2,50}$";
    public static final String JATO_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ 0-9 ,]{2,250}$";
    
    
    /**
     * vrijeme
     */
    public static final String FORMAT_DATUMA="dd. MM. yyyy.";
    
    /**
     * kontrola bolesti
     */
    public static final String BROJ_ZARAZENIH_PATTERN = "^[0-9]{1,11}$";
    public static final String NAZIV_BOLESTI_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ]{2,50}$";
    public static final String NAZIV_LIJEK_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ ,]{2,50}$";
    

    
    
}

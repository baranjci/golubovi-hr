/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.utility;

import golubovi.controller.ObradaProgramer_program;
import golubovi.model.Programer_program;

/**
 *
 * @author Izidora i Mario
 */
public class OProgramu extends javax.swing.JFrame {
    
    Programer_program pp;
    ObradaProgramer_program obradaProgramer_program;

    /**
     * Creates new form OProgramu
     */
    public OProgramu() {
        initComponents();
        obradaProgramer_program = new ObradaProgramer_program();
        traziPodatkeOProgramu();
    }

    private void traziPodatkeOProgramu() {
        
        pp = obradaProgramer_program.dohvatiIzBaze(null).get(0);
         
        lblProizvodjac.setText("proizvodjac: " + pp.getProizvodjac());
        
        lblProjekt.setText("projekt: " + pp.getProjekt());
        
        lblVerzija.setText("verzija: " + pp.getVerzija_programa());
        
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblVerzija = new javax.swing.JLabel();
        lblProjekt = new javax.swing.JLabel();
        lblProizvodjac = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("O programu"));

        lblVerzija.setText("verzija:");

        lblProjekt.setText("projekt:");

        lblProizvodjac.setText("proizvođač:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblProizvodjac)
                    .addComponent(lblVerzija)
                    .addComponent(lblProjekt))
                .addContainerGap(136, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblVerzija)
                .addGap(18, 18, 18)
                .addComponent(lblProjekt)
                .addGap(20, 20, 20)
                .addComponent(lblProizvodjac)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(250, 192));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblProizvodjac;
    private javax.swing.JLabel lblProjekt;
    private javax.swing.JLabel lblVerzija;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package golubovi.view;

import golubovi.controller.ObradaJedinke;
import golubovi.controller.ObradaObjekt;
import golubovi.model.Jedinke;
import golubovi.model.Objekt;
import golubovi.utility.OProgramu;
import golubovi.utility.SlanjeEmailPoruka;
import golubovi.utility.TehnickaPodrska;
import java.awt.Color;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Izidora i Mario
 */
public class GlavniIzbornik extends javax.swing.JFrame {
    
    private ObradaJedinke obradaJedinke;
    private ObradaObjekt obradaObjekt;
    private Prijava prijava;
    private Jedinke jedinke;

    /**
     * Creates new form Izbornik
     */
    public GlavniIzbornik(Prijava prijava) {
        initComponents();
        jedinke = new Jedinke();
        obradaJedinke = new ObradaJedinke();
        obradaObjekt = new ObradaObjekt();
        this.prijava = prijava;
        slika();
        kontrolaAdmin();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnGoluboviUBazi = new javax.swing.JButton();
        btnBolesti = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnDodajNovogGoluba = new javax.swing.JButton();
        lblOdjava = new javax.swing.JLabel();
        lblAdmin = new javax.swing.JLabel();
        btnUveziCSV = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jOsobniPodaci = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPodaciOObjektu = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jOProgramu = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jKontaktEmail = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Glavni izbornik");
        setMaximumSize(new java.awt.Dimension(0, 0));
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Glavni izbornik"));

        btnGoluboviUBazi.setText("Pregled jata");
        btnGoluboviUBazi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoluboviUBaziActionPerformed(evt);
            }
        });

        btnBolesti.setText("Pregled bolesti");
        btnBolesti.setName(""); // NOI18N
        btnBolesti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBolestiActionPerformed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon("D:\\net beans workspace\\Golubovi-hr\\slike\\slikaIzbornik\\golub.jpg")); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(346, 300));
        jLabel1.setMinimumSize(new java.awt.Dimension(346, 183));

        btnDodajNovogGoluba.setText("Dodaj novog goluba u jato");
        btnDodajNovogGoluba.setName(""); // NOI18N
        btnDodajNovogGoluba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDodajNovogGolubaActionPerformed(evt);
            }
        });

        lblOdjava.setForeground(new java.awt.Color(255, 51, 0));
        lblOdjava.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOdjava.setText("Odjava iz programa");
        lblOdjava.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblOdjavaMouseClicked(evt);
            }
        });

        lblAdmin.setText("Admin");

        btnUveziCSV.setText("Uvezi CSV");
        btnUveziCSV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUveziCSVActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnUveziCSV, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblAdmin, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                        .addComponent(btnGoluboviUBazi, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                        .addComponent(btnDodajNovogGoluba, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                        .addComponent(btnBolesti, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(lblOdjava)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOdjava, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAdmin))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnGoluboviUBazi, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDodajNovogGoluba, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnBolesti, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(btnUveziCSV, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jButton1.setText("jButton1");

        jOsobniPodaci.setText("O vlasniku");

        jMenuItem1.setText("Osobni podaci");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jOsobniPodaci.add(jMenuItem1);

        jMenuBar1.add(jOsobniPodaci);

        jPodaciOObjektu.setText("O objektu");

        jMenuItem2.setText("Podaci o objektu");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPodaciOObjektu.add(jMenuItem2);

        jMenuBar1.add(jPodaciOObjektu);

        jOProgramu.setText("O programu");

        jMenuItem3.setText("O programu");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jOProgramu.add(jMenuItem3);

        jMenuItem4.setText("Podrška i uklanjanje poteškoća");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jOProgramu.add(jMenuItem4);

        jKontaktEmail.setText("Kontakt e-mail");
        jKontaktEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jKontaktEmailActionPerformed(evt);
            }
        });
        jOProgramu.add(jKontaktEmail);

        jMenuBar1.add(jOProgramu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        setSize(new java.awt.Dimension(605, 421));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * otvara prozor s tehničkim podacima o programu
     *
     * @param evt
     */
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new OProgramu().setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed
    /**
     * otvara prozor tehničke podrške (e-mail, br.telefona)
     *
     * @param evt
     */
    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        new TehnickaPodrska().setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    /**
     * otvara prozor s podacima o vlasniku koji su povučeni iz baze
     *
     * @param evt
     */
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        new PodaciOVlasniku().setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed
    /**
     * otvara prozor s podacima o objektu koji su povučeni iz baze
     *
     * @param evt
     */
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new PodaciOObjektu().setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void btnGoluboviUBaziActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoluboviUBaziActionPerformed
        
        List<Jedinke> lista = obradaJedinke.dohvatiSifruIzBaze();
        DefaultListModel m = new DefaultListModel<>();
        
        for (Jedinke j : lista) {
            m.addElement(j);
        }
        if (m.isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Nema golubova u bazi!", "Greška", JOptionPane.ERROR_MESSAGE);
        } else {
            new PodaciOGolubovima(null).setVisible(true);
        }
    }//GEN-LAST:event_btnGoluboviUBaziActionPerformed

    private void btnBolestiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBolestiActionPerformed
        new PregledBolesti().setVisible(true);
    }//GEN-LAST:event_btnBolestiActionPerformed

    private void btnDodajNovogGolubaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDodajNovogGolubaActionPerformed
        List<Objekt> lista = obradaObjekt.dohvatiSifruIzBaze();
        DefaultListModel m = new DefaultListModel<>();
        
        for (Objekt o : lista) {
            m.addElement(o);
        }
        if (m.isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Nema objekata u bazi! \nMolimo dodajte objekte!", "Greška", JOptionPane.ERROR_MESSAGE);
            new DodajNoviObjekat(null).setVisible(true);
        } else {
            new DodajNovogGoluba().setVisible(true);
        }
    }//GEN-LAST:event_btnDodajNovogGolubaActionPerformed

    private void jKontaktEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jKontaktEmailActionPerformed
        new SlanjeEmailPoruka().setVisible(true);
    }//GEN-LAST:event_jKontaktEmailActionPerformed

    private void lblOdjavaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOdjavaMouseClicked
        setVisible(false);
        new Prijava().setVisible(true);
    }//GEN-LAST:event_lblOdjavaMouseClicked

    private void btnUveziCSVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUveziCSVActionPerformed
        importCSV();
    }//GEN-LAST:event_btnUveziCSVActionPerformed

    /**
     * @param args the command line arguments
     */
    
    private void importCSV() {
        
        JFileChooser jfc = new JFileChooser();
        jfc.setApproveButtonText("Uvezi");
        File startFile = new File(System.getProperty("user.home") + File.separator + "Desktop");
        jfc.setCurrentDirectory(startFile);
        FileFilter filter = new FileNameExtensionFilter("Podaci odvojeni razdjelnikom", "csv");
        jfc.setFileFilter(filter);
        int rez = jfc.showOpenDialog(this.getParent());
        if (rez == JFileChooser.APPROVE_OPTION) {

            try {
                // Open the file that is the first 
                // command line parameter
                FileInputStream fstream = new FileInputStream(jfc.getSelectedFile().getAbsolutePath());
                // Get the object of DataInputStream
                DataInputStream in = new DataInputStream(fstream);
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String strLine;
                //Read File Line By Line
                String[] niz;
                while ((strLine = br.readLine()) != null) {
                    // Print the content on the console
                    niz = strLine.split(";");
                   jedinke = new Jedinke();
                    jedinke.setBroj_prstena(niz[0]);
                    jedinke.setBoja(niz[1]);
                    jedinke.setJato(niz[2]);
                    Objekt o = new Objekt();
                    o.setSifra(Integer.parseInt(niz[3]));
                    jedinke.setObjekt(o);
                    Date datum = new Date (Integer.parseInt(niz[4]));
                    jedinke.setRodjenje(datum);
                   jedinke.setSpol(niz[5]);
                   jedinke.setSlika(niz[6]);
                  jedinke = obradaJedinke.dodajNovi(jedinke);
                }
              
                //Close the input stream
                in.close();
            } catch (Exception e) {//Catch exception if any
                System.err.println("Error: " + e.getMessage());
            }

        }
    }
    
    private void slika() {
        
        try {
            
            ImageIcon background = new ImageIcon("slike" + File.separator + "slikaIzbornik" + File.separator + "golub.jpg");
            Image slika = background.getImage();
            Image novaSlika = slika.getScaledInstance(300, 192, java.awt.Image.SCALE_SMOOTH);
            background = new ImageIcon(novaSlika);
            jLabel1.setIcon(background);
            jLabel1.setText("");
            
        } catch (Exception e) {
        }
        
    }
    
    private void kontrolaAdmin() {
        
        try {
            
            if (prijava.getPp().getKorisnicko_ime_admin() != null) {
                lblAdmin.setText("Prijavljeni ste kao administrator!");
                lblAdmin.setForeground(Color.red);
                
            } else {
                
            }
        } catch (NullPointerException e) {
            lblAdmin.setVisible(false);
        }
        
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBolesti;
    private javax.swing.JButton btnDodajNovogGoluba;
    private javax.swing.JButton btnGoluboviUBazi;
    private javax.swing.JButton btnUveziCSV;
    private javax.swing.JButton jButton1;
    private javax.swing.JMenuItem jKontaktEmail;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenu jOProgramu;
    private javax.swing.JMenu jOsobniPodaci;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JMenu jPodaciOObjektu;
    private javax.swing.JLabel lblAdmin;
    private javax.swing.JLabel lblOdjava;
    // End of variables declaration//GEN-END:variables

}

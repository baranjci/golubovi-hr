/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package golubovi;

import golubovi.controller.ObradaVlasnik;
import golubovi.model.Vlasnik;
import golubovi.view.Prijava;
import golubovi.view.Registracija;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;

/**
 *
 * @author Izidora i Mario
 */
public class GoluboviStart {

    public static void main(String[] args) {

        JFrame.setDefaultLookAndFeelDecorated(true);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    // poziva se look and feel kroz ui manager - zadnji dio u navodnicima (u zagradi) je naziv skina - imate sve nazive na onim linkovima gore  
                    UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceNebulaBrickWallLookAndFeel");
                } catch (Exception e) {
                    System.out.println("skin not loaded");
                }

//colorization factor ne morate stavljati, ja sam ga stavio jer po defaultu sve boje stavlja na 50% prozirnosti, dakle factor je po defaultu 0.5, a na 1.0 ne stavlja prozirnost uopće - meni je to više odgovaralo pa sam ipak stavio
                UIManager.put(SubstanceLookAndFeel.COLORIZATION_FACTOR, 1.0);

                ObradaVlasnik obradaVlasnik = new ObradaVlasnik();
                Vlasnik vlasnik = null;
                try {

                    vlasnik = obradaVlasnik.dohvatiIzBaze(null).get(0);

                } catch (IndexOutOfBoundsException e) {

                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Nema konfiguracijske datoteke", "Greška", JOptionPane.ERROR_MESSAGE);
                    return;
                } catch (ClassNotFoundException | IOException ex) {
                    JOptionPane.showMessageDialog(null, "Driver za bazu nije u classpath", "Greška", JOptionPane.ERROR_MESSAGE);
                    return;

                } catch (SQLException ex) {
                    switch (ex.getErrorCode()) {
                        case 0:
                            JOptionPane.showMessageDialog(null, "Server nedostupan", "Greška", JOptionPane.ERROR_MESSAGE);
                            break;
                        case 1049:
                            JOptionPane.showMessageDialog(null, "Baza ne postoji na serveru", "Greška", JOptionPane.ERROR_MESSAGE);
                            break;
                        default:
                            JOptionPane.showMessageDialog(null, ex.getMessage() + ".\nMolimo kontaktirajte proizvođača", "Greška", JOptionPane.ERROR_MESSAGE);
                            break;

                    }
                    return;
                }

                if (vlasnik == null) {

                    new Registracija(null).setVisible(true);

                } else {

                    new Prijava().setVisible(true);
                }

           
            
            
            }
            
        });
    }
}
                
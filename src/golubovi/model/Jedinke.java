/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.model;

import java.sql.Date;

/**
 *
 * @author Izidora i Mario
 */
public class Jedinke {
    
    
    private int sifra;
    private String broj_prstena;
    private String spol;
    private Objekt objekt; 
    private Date rodjenje;
    private String slika;
    private String boja;
    private String jato;
    
  
    public Objekt getObjekt() {
        return objekt;
    }

    public void setObjekt(Objekt objekt) {
        this.objekt = objekt;
    }

    public String getJato() {
        return jato;
    }

    public void setJato(String jato) {
        this.jato = jato;
    }

    public String getBoja() {
        return boja;
    }

    public void setBoja(String boja) {
        this.boja = boja;
    }

    public String getBroj_prstena() {
        return broj_prstena;
    }

    public void setBroj_prstena(String broj_prstena) {
        this.broj_prstena = broj_prstena;
    }
     
    public String getSpol() {
        return spol;
    }

    public void setSpol(String spol) {
        this.spol = spol;
    }    
   
    public Date getRodjenje() {
        return rodjenje;
    }

    public void setRodjenje(Date rodjenje) {
        this.rodjenje = rodjenje;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }
    public int getSifra() {
        return sifra;
    }

    public void setSifra(int sifra) {
        this.sifra = sifra;
    }
    
    @Override
     public String toString(){
        return this.broj_prstena;
     }

   
    
    
}

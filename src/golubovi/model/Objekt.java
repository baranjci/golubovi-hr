/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package golubovi.model;

/**
 *
 * @author Izidora i Mario
 */
public class Objekt {

    private int sifra;
    private String lokacija;
    private int vlasnik;
    private int velicina;
    private int broj_prostorija;
    private int broj_etaza;
    private String naziv;

    

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getLokacija() {
        return lokacija;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }

    public int getVlasnik() {
        return vlasnik;
    }

    public void setVlasnik(int vlasnik) {
        this.vlasnik = vlasnik;
    }

    public int getVelicina() {
        return velicina;
    }

    public void setVelicina(int velicina) {
        this.velicina = velicina;
    }

    public int getBroj_prostorija() {
        return broj_prostorija;
    }

    public void setBroj_prostorija(int broj_prostorija) {
        this.broj_prostorija = broj_prostorija;
    }

    public int getBroj_etaza() {
        return broj_etaza;
    }

    public void setBroj_etaza(int broj_etaza) {
        this.broj_etaza = broj_etaza;
    }

    public int getSifra() {
        return sifra;
    }

    public void setSifra(int sifra) {
        this.sifra = sifra;
    }

    
    
    @Override
     public String toString(){
        return this.naziv ;
     }
    

    public int getVelicinaUkupno() {
        
        int a = velicina;
        int b = broj_etaza;
                
        int rez = a*b; 
        return rez;
        
    }

}

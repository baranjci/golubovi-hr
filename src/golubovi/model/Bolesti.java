/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.model;

import java.sql.Date;

/**
 *
 * @author Izidora i Mario
 */
public class Bolesti {
    
    private int sifra;
    private String lijek;

    public String getLijek() {
        return lijek;
    }

    public void setLijek(String lijek) {
        this.lijek = lijek;
    }
   
    private String naziv;

    public Date getTrajanje_bolesti_od() {
        return trajanje_bolesti_od;
    }

    public void setTrajanje_bolesti_od(Date trajanje_bolesti_od) {
        this.trajanje_bolesti_od = trajanje_bolesti_od;
    }

    public Date getTrajanje_bolesti_do() {
        return trajanje_bolesti_do;
    }

    public void setTrajanje_bolesti_do(Date trajanje_bolesti_do) {
        this.trajanje_bolesti_do = trajanje_bolesti_do;
    }
    private Date trajanje_bolesti_od;
    private Date trajanje_bolesti_do;
    private int broj_zarazenih;

    

    public int getBroj_zarazenih() {
        return broj_zarazenih;
    }

    public void setBroj_zarazenih(int broj_zarazenih) {
        this.broj_zarazenih = broj_zarazenih;
    }
    
      public int getSifra() {
        return sifra;
    }

    public void setSifra(int sifra) {
        this.sifra = sifra;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
     public String toString(){
        return this.naziv;
     }
    
}

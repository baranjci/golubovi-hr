/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package golubovi.model;



/**
 *
 * @author Izidora i Mario
 */
public class Programer_program {
    
    private String ime_programer;
    private String prezime_programer;
    private String email;
    private String mobitel;
    private String verzija_programa;
    private String projekt;
    private String proizvodjac;

    public String getKorisnicko_ime_admin() {
        return korisnicko_ime_admin;
    }

    public void setKorisnicko_ime_admin(String korisnicko_ime_admin) {
        this.korisnicko_ime_admin = korisnicko_ime_admin;
    }

    public String getLozinka_admin() {
        return lozinka_admin;
    }

    public void setLozinka_admin(String lozinka_admin) {
        this.lozinka_admin = lozinka_admin;
    }
    private String korisnicko_ime_admin;
    private String lozinka_admin;

    public String getIme_programer() {
        return ime_programer;
    }

    public void setIme_programer(String ime_programer) {
        this.ime_programer = ime_programer;
    }

    public String getPrezime_programer() {
        return prezime_programer;
    }

    public void setPrezime_programer(String prezime_programer) {
        this.prezime_programer = prezime_programer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobitel() {
        return mobitel;
    }

    public void setMobitel(String mobitel) {
        this.mobitel = mobitel;
    }

    public String getVerzija_programa() {
        return verzija_programa;
    }

    public void setVerzija_programa(String verzija_programa) {
        this.verzija_programa = verzija_programa;
    }

    public String getProjekt() {
        return projekt;
    }

    public void setProjekt(String projekt) {
        this.projekt = projekt;
    }

    public String getProizvodjac() {
        return proizvodjac;
    }

    public void setProizvodjac(String proizvodjac) {
        this.proizvodjac = proizvodjac;
    }
    
            
    
}

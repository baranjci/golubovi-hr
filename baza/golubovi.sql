drop database if exists golubovi;
create database golubovi character set utf8 collate utf8_general_ci;
use golubovi;

create table objekt(
	sifra				int not null primary key auto_increment,
	naziv			 	varchar(50) not null,
	lokacija			varchar(50) not null,
	vlasnik				int not null,
	velicina			int not null,
	broj_prostorija                 int not null,
	broj_etaza			int not null
)engine=innodb;

create table jedinke(
	sifra				int not null primary key auto_increment,
        broj_prstena                    varchar(250) not null,
	boja                            varchar(50) not null,				
	spol		 		varchar(11) not null,
	datum_rodjenja                  datetime,
	slika				varchar (255),
        objekt                          int not null,
        jato                            varchar (250) not null
)engine=innodb;

create table vlasnik(
	sifra				int not null primary key auto_increment,
        korisnicko_ime                  varchar(8) not null,
        lozinka                         char(32) not null,
	ime				varchar(50) not null,
	prezime				varchar(50) not null,
	adresa     			varchar(50) not null,
        email				varchar(100) 
)engine=innodb;

create table bolesti(
	sifra 				int not null primary key auto_increment,
	naziv 				varchar(50) not null,
	trajanje_bolesti_od               datetime not null,
        trajanje_bolesti_do               datetime not null,
	broj_zarazenih                  int not null,
        lijek                           varchar(50) not null
)engine=innodb;

create table jedinke_bolesti(
        jedinke                         int not null,
        bolesti                         int not null
)engine=innodb;

create table programer_program(
    sifra 				int not null primary key auto_increment,
        ime_programer                   varchar(50) not null,
        prezime_programer               varchar(50) not null,
        email                           varchar(250) not null,
        mobitel                         varchar(20) not null,
        verzija_programa                varchar(250) not null,
        projekt                         varchar(250) not null,
        proizvodjac                     varchar(250) not null,
        korisnicko_ime_admin            varchar(20) not null,
        lozinka_admin                   char(32) not null
)engine=innodb;


alter table objekt add foreign key (vlasnik) references vlasnik (sifra);
alter table jedinke add foreign key (objekt) references objekt (sifra);
alter table jedinke_bolesti add foreign key (jedinke) references jedinke(sifra);
alter table jedinke_bolesti add foreign key (bolesti) references bolesti(sifra);

#1
insert into programer_program (ime_programer, prezime_programer, email, mobitel,verzija_programa,
projekt, proizvodjac, korisnicko_ime_admin, lozinka_admin) values ('Mario','Kušen','golubovi.hr@gmail.com','+385981384674','1.0.0.','java probni projekti',
'homePrograming', 'admin', md5('admin'));